/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */

#ifndef __LIBIRC_CONST_H
#define __LIBIRC_CONST_H


/*
 * maximum buffer sizes as specified by RFC 1459.
 */
static const size_t     IRC_MSG = 510;
static const size_t     IRC_NICK = 9;
static const size_t     IRC_FQDN = 253;
static const size_t     IRC_HOST = 63;
static const size_t     IRC_CHAN = 50;

static const uint16_t   IRC_PORT = 6667;
static const uint16_t	IRC_SSL_PORT = 6697;

/* delay in milliseconds to use when draining receive buffers */
static const int        IRC_DRAIN_DELAY = 2500;

/* default string used for IRC PONG replies */
static const char       IRC_PONG_DAEMON[] = "libirc client";


#endif
