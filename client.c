/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */

#include <sys/types.h>
#include <sys/socket.h>

#include <err.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include "irc.h"

#define IRC_MSG 512


/*
 * implement a simple IRC client
 */
int
main(void)
{
        struct irc_client   *irc;
        char                buf[IRC_MSG + 1];

        printf("[+] creating irc client...\n"); 
        if (NULL == (irc = irc_create(AF_INET)))
            err(EX_USAGE, "failed to create IRC client");

        printf("[+] configuring client...\n");
        irc->server = strdup("irc.freenode.net");
        irc->port = (uint16_t)6667;
        irc->nick = strdup("libirctst");
        irc->host = strdup("localhost");
        irc->sys = strdup("localhost");
        irc->user = strdup("libirctst");
        irc->realname = strdup("kyle isom");
        irc->password = NULL;

        printf("[+] connecting...\n");
        if (-1 == irc_connect(irc))
               err(EX_OSERR, "failed to connect"); 

        printf("[+] receiving...\n");
        if (-1 == irc_recv(irc, buf, IRC_MSG, 1000))
            warn("[!] receive failed");
        else {
                printf("[+] received: '%s'\n", buf);
        }

        irc_msg(irc, "kyl", "hello world");
        irc_join(irc, "#gokyletests");
        irc_msg(irc, "#gokyletests", "testing 1 2 3");
        printf("parting\n");
        irc_part(irc, "#gokyletests");
        sleep(30);
        irc_quit(irc);
        printf("[+] destroying irc client...\n");
        if (irc_destroy(irc) != 0)
            fprintf(stderr, "[!] error destroying irc client\n");
        

        return 0;
}
