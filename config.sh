#!/bin/sh

skipnext=0
SSL="no"
for opt in $@
do
	if [ $skipnext -eq 1 ]; then
		skipnext=0
		continue
	fi

	if [ "$opt" = "--no-ssl" ]; then
		SSL="no"
        elif [ "$opt" = "-ssl" ]; then
                SSL="yes"
	fi
done

if [ "${SSL}" = "yes" ]; then
	echo "[+] will include SSL support"
	SSL_LIBS="-lcrypto -lssl -lpthread"
	SSL_CFLAG="-DUSE_OPENSSL"
        SSL_CLIENT="ssl_client"
else
	echo "[+] won't include SSL support"
	SSL_LIBS=""
	SSL_CFLAG=""
        SSL_CLIENT=""
fi

OPSYS=$(uname -s)
echo "Configuring for ${OPSYS}..."
if [ "x${OPSYS}" = "xLinux" ]; then
    OS_CFLAGS="-D_BSD_SOURCE -D_POSIX_SOURCE -D_XOPEN_SOURCE"
    OS_CFLAGS="${OS_CFLAGS} -D__USE_XOPEN2K -D_MULTI_THREADED"
    OS_CFLAGS="${OS_CFLAGS} -D_NO_TIMEDLOCK"
elif [ "x${OPSYS}" = "xDarwin" ]; then
    OS_CFLAGS="-D_NO_TIMEDLOCK"
else
    OS_CFLAGS=""
fi

if [ -z "${OS_CFLAGS}" ]; then
    echo "${OPSYS} requires no extra build flags."
else
    echo "${OPSYS} requires build flags ${OS_CFLAGS}"
fi

if [ -z "${PREFIX}" ]; then
    PREFIX="/usr/local"
fi    

if [ "${PREFIX}" = "/usr" ]; then
    MANDIR="$(PREFIX)/share/man"
elif [ "${PREFIX}" = "/usr/local" ]; then
    if [ "${OPSYS}" = "Darwin" ]; then
        MANDIR="${PREFIX}/share/man"
    else
        MANDIR="${PREFIX}/man"
    fi
else
    MANDIR="${PREFIX}/man"
fi

echo "prefix: ${PREFIX}"
echo "mandir: ${MANDIR}"

echo "writing new Makefile"
rm -f Makefile
cat Makefile.in | sed -e "s|<OS_CFLAGS>|${OS_CFLAGS}|"          \
                | sed -e "s|\$PREFIX|${PREFIX}|"                \
                | sed -e "s|\$MANDIR|${MANDIR}|"                \
                | sed -e "s|<SSL>|${SSL_LIBS}|"                 \
		| sed -e "s|<SSL_CFLAG>|${SSL_CFLAG}|"          \
                | sed -e "s|<SSLCLIENT>|${SSL_CLIENT}|"         \
	>Makefile

echo "done."
