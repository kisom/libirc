/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */

#ifndef __LIBIRC_IRC_H
#define __LIBIRC_IRC_H

#include <sys/queue.h>
#include <pthread.h>
#include <stdint.h>

struct channel {
        char                    *name;
        TAILQ_ENTRY(channel)    channels;
};
TAILQ_HEAD(tq_chanhead, channel);
typedef struct tq_chanhead * tq_chanheadp;

struct s_irclk {
        pthread_mutex_t mtx;
        struct timespec block;
};

struct irc_client {
        char            *server;
        uint16_t        port;
        int             proto;
        char            *nick;
        char            *host;
        char            *sys;
        char            *user;
        char            *realname;
        tq_chanheadp    chanlist;
        char            *password;
        struct s_irclk  lock;
        int             reconnect;
        int             connected;
	int		ssl;
	union {
		int     sock;
	}		conn;
};

struct irc_client	*irc_create(int);
int			irc_connect(struct irc_client *);
int			irc_destroy(struct irc_client *);
ssize_t			irc_send(struct irc_client *, const char *);
ssize_t			irc_recv(struct irc_client *, char *, size_t, int);
int			irc_pong(struct irc_client *, const char *);
int			irc_msg(struct irc_client *, const char *, 
                            const char *);
int			irc_quit(struct irc_client *);
int			irc_join(struct irc_client *, const char *);
int			irc_part(struct irc_client *, const char *);
char			*irc_userline(struct irc_client *);
int			irc_identify(struct irc_client *);
char			*irc_connstring(struct irc_client *);

#endif
