/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */

#include <sys/types.h>
#include <sys/queue.h>
#include <sys/socket.h>

#include <err.h>
#include <errno.h>
#include <poll.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "const.h"
#include "irc.h"
#include "connect.h"


extern const size_t     IRC_MSG;
extern const size_t     IRC_NICK;
extern const size_t     IRC_FQDN;
extern const size_t     IRC_HOST;
extern const size_t     IRC_CHAN;
extern const uint16_t   IRC_PORT;
extern const int        IRC_DRAIN_DELAY;
extern const char       IRC_PONG_DAEMON[];
static const time_t     IRC_TBLK_S = 0;
static const long       IRC_TBLK_NS = 250;


static int		socket_init(struct irc_client *, int);
static int		socket_shutdown(struct irc_client *);
static int		socket_closed(int);
static int              acquire_lock(struct s_irclk *, int);


/*
 * socket_init sets up the IRC connection's socket.
 */
static int
socket_init(struct irc_client *irc, int proto)
{
        int error;

        error = 0;
        irc->conn.sock = socket(proto, SOCK_STREAM, 0);
        if (irc->conn.sock == -1)
                error = -1;

        return error;
}


/*
 * socket_shutdown tears down the IRC connection's socket.
 */
static int
socket_shutdown(struct irc_client *irc)
{
        int error;

        if ((error = shutdown(irc->conn.sock, SHUT_RDWR)) != 0) {
                warn("error shutting down socket");
        } else {
                error = close(irc->conn.sock);
        } 
        return error;
}


/*
 * irc_create creates a new irc_client.
 */
struct irc_client *
irc_create(int proto)
{
        struct irc_client *irc;

        if (proto != AF_INET && proto != AF_INET6)
            return NULL;

        irc = NULL;
        if (NULL == (irc = calloc(1, sizeof(struct irc_client)))) {
                warn("failed to allocate memory for irc struct");
        } else if (NULL == (irc->server = calloc(IRC_FQDN + 1, sizeof(char)))) {
                warn("failed to allocate memory for server name");
        } else if (NULL == (irc->nick = calloc(IRC_NICK + 1, sizeof(char)))) {
                warn("failed to allocate memory for nick");
        } else if (NULL == (irc->host = calloc(IRC_HOST + 1, sizeof(char)))) {
                warn("failed to allocate memory for host");
        } else if (NULL == (irc->realname = calloc(IRC_HOST + 1, 
                            sizeof(char)))) {
                warn("failed to allocate memory for realname");
        } else if (NULL == (irc->password = calloc(IRC_HOST + 1,
                            sizeof(char)))) {
                warn("failed to allocate memory for password");
        } else if (NULL == (irc->chanlist = calloc(1, 
                            sizeof(struct tq_chanhead)))) {
                warn("failed to allocate memory for channel list");
        } else {
                irc->conn.sock = 0;

                if (0 != (pthread_mutex_init(&irc->lock.mtx, NULL)))
                        warnx("failed to initialise mutex");
                irc->lock.block.tv_sec = IRC_TBLK_S;
                irc->lock.block.tv_nsec = IRC_TBLK_NS;
		irc->conn.sock = 0;
                irc->reconnect = 0;
                irc->connected = 0;
                TAILQ_INIT(irc->chanlist);
                irc->proto = proto;
        }


        return irc;
}


/*
 * irc_destroy properly frees an IRC client connection. irc_destroy
 * returns -1 on error, and 0 on success. It is important that clients
 * call this function to properly deallocate all resources associated
 * with the connection.      
 */
int
irc_destroy(struct irc_client *irc)
{
        struct channel  *chan;
        int             error = -1;

        if (0 == (error = acquire_lock(&irc->lock, 0))) {
                error = socket_shutdown(irc);

                free(irc->server);
                free(irc->nick);
                free(irc->host);
                free(irc->sys);
                free(irc->user);
                free(irc->realname);
                free(irc->password);
                irc->server = NULL;
                irc->nick = NULL;
                irc->host = NULL;
                irc->sys = NULL;
                irc->user = NULL;
                irc->realname = NULL;
                irc->password = NULL;

                while ((chan = TAILQ_FIRST(irc->chanlist))) {
                        free(chan->name);
                        TAILQ_REMOVE(irc->chanlist, chan, channels);
                        free(chan);
                }
                error = pthread_mutex_unlock(&irc->lock.mtx);
        }

        if (error == 0) {
                warnx("destroying mutex and freeing memory");
                error = pthread_mutex_destroy(&irc->lock.mtx);
                free(irc);
                irc = NULL;
        }
        return error; 
}


/*
 * irc_send will properly send an IRC messages to the server; it will
 * ensure the message is appropriately sized and has the proper line
 * endings.
 */
ssize_t
irc_send(struct irc_client *irc, const char *msg)
{
        char        *msgbuf;
        size_t      msg_sz;
        size_t      msgbuf_sz;
        ssize_t     send_sz;

        msg_sz = strlen(msg);
        if (msg_sz > (IRC_MSG - 2))
                warn("message is too long");

        msgbuf = NULL;
        if ((msgbuf = calloc(IRC_MSG + 1, sizeof(char))) == NULL) {
                warn("failed to allocate memory for message!");
                return -1;
        }

        snprintf(msgbuf, IRC_MSG, "%s\r\n", msg);
        msgbuf_sz = strlen(msgbuf);
        if (msg_sz != (msgbuf_sz - 2)) {
                warnx("invalid message size");
        } 

        if (0 == (send_sz = acquire_lock(&irc->lock, 0))) {
                send_sz = send(irc->conn.sock, msgbuf, strlen(msgbuf), 0);
                pthread_mutex_unlock(&irc->lock.mtx);

                if (-1 == send_sz) {
                        warn("send failure");
                        if (socket_closed(errno))
                                irc->connected = 0;

                        if (!irc->connected && irc->reconnect)
                                send_sz = irc_connect(irc);
                }
        } else {
                send_sz = -1;
        }
        free(msgbuf);
        msgbuf = NULL;
        return send_sz;
}


/*
 * irc_recv receives a message from the server. it returns -1 on error,
 * and otherwise the amount of data read from the socket. if a timeout
 * occurs, it will return 0 (no data read). note that timeout should be
 * a suitable poll(2) value: -1 to block, 0 to only receive data if
 * there is data on the wire, and a positive integer specifying the 
 * timeout in milliseconds.
 */
ssize_t
irc_recv(struct irc_client *irc, char *buf, size_t buflen, int timeout)
{
        struct pollfd    fds[1];
        ssize_t          recvsize;
        int              pollev;

        fds[0].fd = irc->conn.sock;
        fds[0].events = POLLIN;
        recvsize = 0;

        if (buf == NULL || buflen == 0) {
                warn("no memory allocated for buffer");
                return -1;
        }

        if (0 == (recvsize = acquire_lock(&irc->lock, 0))) {
                pollev = poll((struct pollfd *)&fds, (nfds_t)1, timeout);
                pthread_mutex_unlock(&irc->lock.mtx);
        } else {
                return -1;
        }

        if (pollev == -1 || fds[0].revents & (POLLERR|POLLHUP|POLLNVAL)) {
                warn("socket poll failure");
                recvsize = -1;
        } else if (pollev == 1) {
                recvsize = recv(irc->conn.sock, buf, buflen, 0);
                if (recvsize == 0) {
                        irc->connected = 0;
                        warnx("client not connected");
                        if (irc->reconnect == 1)
                                recvsize = irc_connect(irc);
                        else
                                recvsize = -1;
                }
        }
        return recvsize;
}


/*
 * irc_pong sends a pong reply to the server. if a NULL daemon value is
 * passed in, irc_pong will substitute the text in PONG_DEFAULT for the
 * pong value.
 */
int
irc_pong(struct irc_client *irc, const char *pongid)
{
        int     error;
        char    *msg;

        msg = NULL;

        if (NULL == (msg = calloc(IRC_MSG + 1, sizeof(char)))) {
                warn("failed to allocate memory for message");
                error = -1;
        } else {
                snprintf(msg, IRC_MSG, "PONG %s",
                    pongid == NULL ? IRC_PONG_DAEMON : pongid);
                error = irc_send(irc, msg);
        }

        free(msg);
        msg = NULL;

        return error;
}


/*
 * irc_join causes the client to join the specified channel. it returns
 * -1 on error and 0 on success.
 */
int
irc_join(struct irc_client *irc, const char *channm)
{
        struct channel  *chan;
        char            *jstr;
        int             error; 

        if (NULL == (jstr = calloc(IRC_MSG + 1, sizeof(char)))) {
                warn("failed to allocate memory for message");
                return -1;
        } else if (NULL == (chan = calloc(1, sizeof (struct channel)))) {
                warn("failed to allocate memory for new channel");
                free(jstr);
                jstr = NULL;
                return -1;
        } else if (NULL == (chan->name = calloc(IRC_CHAN, sizeof(char)))) {
                warn("failed to allocate memory for channel name");
                free(jstr);
                free(chan);
                jstr = NULL;
                chan = NULL;
                return -1;
        }

        snprintf(jstr, IRC_MSG, "JOIN %s", channm);
        error = irc_send(irc, jstr);
        if (error > 0) {
                strlcpy(chan->name, channm, IRC_CHAN);
                TAILQ_INSERT_HEAD(irc->chanlist, chan, channels);
        } else {
                printf("error joining channel");
                free(chan->name);
                chan->name = NULL;
                free(chan);
                chan = NULL;
        } 

        free(jstr);
        jstr = NULL;
        return error;
}


/*
 * irc_part leaves a channel.
 */
int
irc_part(struct irc_client *irc, const char *channm)
{
        struct channel  *chan;
        int             error, found;
        char            *pstr;

        error = 0;
        found = 0;
        if (NULL == (pstr = calloc(IRC_MSG + 1, sizeof(char)))) {
                warn("failed to allocate memory for message");
                return -1;
        }
        snprintf(pstr, IRC_MSG, "PART %s", channm);

        TAILQ_FOREACH(chan, irc->chanlist, channels) {
                if (0 == strncmp(channm, chan->name, IRC_CHAN)) {
                        found = 1;
                        break;
                }
        }

        if (found) {
                if (-1 == (error = irc_send(irc, pstr))) {
                        warn("failed to send part message");
                } else {
                        free(chan->name);
                        TAILQ_REMOVE(irc->chanlist, chan, channels);
                        free(chan);
                }
        }

        free(pstr);
        pstr = NULL;
        return error;
}


/*
 * irc_msg sends a private message. it returns -1 on error, and 0 on
 * success.
 */
int
irc_msg(struct irc_client *irc, const char *to, const char *msg)
{
        int     error;
        char    *pm;

        if (NULL == (pm = calloc(IRC_MSG + 1, sizeof(char)))) {
                warn("failed to allocate memory for message");
                return -1;
        }
        snprintf(pm, IRC_MSG, "PRIVMSG %s :%s", to, msg);
        error = irc_send(irc, pm);

        free(pm);
        pm = NULL;
        return error;
}


/*
 * irc_quit sends a quit message to the irc server. returns -1 on error
 * and 0 on success.
 */
int
irc_quit(struct irc_client *irc)
{
        int     error;

        error = irc_send(irc, "QUIT");
        return error;
}


/*
 * socket_closed returns 1 if errno indicates a closed socket, and
 * 0 if it's still open.
 */
static int
socket_closed(int errval)
{ 
        if (errval == EHOSTUNREACH || errval == EHOSTDOWN ||
            errval == ENETDOWN || errval == ECONNREFUSED ||
            errval == ENOPROTOOPT || errval == EDESTADDRREQ)
                return 1;
        else
                return 0;
}


/*
 * userline returns an appropriate userline for connecting to the IRC
 * server.
 */
char *
irc_userline(struct irc_client *irc)
{
        char    *userstr;

        userstr = NULL;
        if (NULL == (userstr = calloc(IRC_MSG + 1, sizeof(char)))) {
                warn("failed to allocate memory for user string");
        } else {
                snprintf(userstr, IRC_MSG, "USER %s %s %s :%s",
                    irc->user, irc->sys, irc->host, irc->realname);
        }
        return userstr;
}


/*
 * irc_identify identifies the client to NickServ.
 */
int
irc_identify(struct irc_client *irc)
{
        char    identity[IRC_MSG + 1];
        int     error;
        
        snprintf(identity, IRC_MSG, "identify %s %s", irc->user, 
            irc->password);
        error = irc_msg(irc, "nickserv", identity);
        return error;
}


/*
 * irc_connect initiates an RFC 1459 connection to an IRC server.
 */
int
irc_connect(struct irc_client *irc)
{
        struct sockaddr     *srv;
        size_t              totrcvd;
        ssize_t             rcvd;
        socklen_t           socklen;
        char                msg[IRC_MSG + 1];
        char                *ustr;
        int                 error;

        socklen = 0;
        if (irc->connected == 1)
                return 0;
        if (-1 == (error = socket_init(irc, irc->proto)))
                return error;

        switch (irc->proto) {
        case AF_INET:
                srv = (struct sockaddr *)load_in(irc->server, irc->port);
                break;
        case AF_INET6:
                srv = (struct sockaddr *)load_in6(irc->server, irc->port);
                break;
        default:
                warn("unsupported protocol %d", irc->proto);
                return -1;
        }

        if (0 == (error = acquire_lock(&irc->lock, 0))) {
                error = connect(irc->conn.sock, (struct sockaddr *)srv, 
                    sizeof(*srv));
                pthread_mutex_unlock(&irc->lock.mtx);
        } else {
                free(srv);
                srv = NULL;
                return -1;
        }
        free(srv);
        srv = NULL;
        if (error == -1) {
                warn("couldn't connect to %s:%u", irc->server, irc->port);
                return -1;
        } 
        rcvd = 1;
        totrcvd = 0;
        while ((rcvd = irc_recv(irc, msg, IRC_MSG, IRC_DRAIN_DELAY)) > 0)
                totrcvd += (size_t)rcvd;

        snprintf(msg, IRC_MSG, "NICK %s", irc->nick);
        if (-1 == irc_send(irc, msg)) {
                warn("failed to send nick");
        }

        rcvd = 1;
        totrcvd = 0;
        while ((rcvd = irc_recv(irc, msg, IRC_MSG, IRC_DRAIN_DELAY)) > 0)
                totrcvd += (size_t)rcvd;

        if (NULL == (ustr = irc_userline(irc)))
                return -1;

        error = irc_send(irc, ustr);
        free(ustr);
        ustr = NULL;
        if (error == -1)
                return -1;

        rcvd = 1;
        totrcvd = 0;
        while ((rcvd = irc_recv(irc, msg, IRC_MSG, IRC_DRAIN_DELAY)) > 0)
                    totrcvd += (size_t)rcvd;
        printf("total received: %lu\n", (long unsigned)totrcvd);

        if (NULL != irc->password && strlen(irc->password) != 0) {
                if (irc_identify(irc) == -1)
                        warn("failed to send identification to services");
        }
        irc->connected = 1;
        return 0;
}


/*
 * acquire_lock attempts to lock the message queue, returning 1 if
 * successful, and 0 if not.
 */
static int
acquire_lock(struct s_irclk *lk, int wait)
{
        int             error;
        struct timeval  timeo = { 0, 10000 };

        do {
                timeo.tv_sec = lk->block.tv_sec;
                timeo.tv_usec = lk->block.tv_nsec * 1000;
                error = pthread_mutex_trylock(&lk->mtx);
                if (EINVAL == error)
                        break;
                else if (error && wait)
                        select(0, NULL, NULL, NULL, &timeo);
                        
        } while (wait && error != 0);

        return error;
}
