/*
 * Copyright (c) 2012 Kyle Isom <kyle@tyrfingr.is>
 * 
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above 
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL 
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 * OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
 * PERFORMANCE OF THIS SOFTWARE. 
 * ---------------------------------------------------------------------
 */


#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <netinet/in.h>

#include <err.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <unistd.h>

#include "connect.h"


/*
 * load an ipv4 address for the hostname
 */
struct sockaddr_in *
load_in(char *hostname, uint16_t port)
{
        struct hostent      *host = NULL;
        struct sockaddr_in  *sa;

        sa = calloc(1, sizeof(struct sockaddr_in));
        if (NULL == sa)
                err(EX_OSERR, "calloc");
        else if ((host = gethostbyname(hostname)) == NULL)
                err(EX_OSERR, "gethostbyname");
        bcopy(host->h_addr, &sa->sin_addr, host->h_length);
        sa->sin_port = htons(port);
        sa->sin_family = AF_INET;

        return sa;
}


/*
 * load an ipv6 address for the hostname
 */
struct sockaddr_in6 *
load_in6(char *hostname, uint16_t port)
{
        struct addrinfo     hints, *res, *cur;
        struct sockaddr_in6 *sa;

        sa = calloc(1, sizeof(struct sockaddr_in6));
        if (NULL == sa)
                err(EX_OSERR, "calloc");

        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET6;
        hints.ai_socktype = SOCK_STREAM;
        if (-1 == getaddrinfo(hostname, NULL, &hints, &res))
                err(EX_OSERR, "getaddrinfo");

        for(cur = res; cur; cur = cur->ai_next) {
                if (cur->ai_socktype != AF_INET6)
                        continue;
                bcopy(res->ai_addr, &sa->sin6_addr, res->ai_addrlen);
                sa->sin6_port = htons(port);
                sa->sin6_family = AF_INET6;
                break;
        }
        return sa;
}

